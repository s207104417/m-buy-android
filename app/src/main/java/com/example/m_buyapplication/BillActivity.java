package com.example.m_buyapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class BillActivity extends AppCompatActivity {

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);

        textView = findViewById(R.id.textView);
        String shop = getIntent().getStringExtra("shop");
        ArrayList<String> foodBill = getIntent().getStringArrayListExtra("foodBill");

        String items = "";
        for (String item: foodBill) {
            items += "\n" + item;
        }
        textView.setText(getString(R.string.shop) + ": " + shop + items);

    }
}