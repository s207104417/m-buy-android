package com.example.m_buyapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class FoodMenuActivity extends AppCompatActivity {

    ListView foodList;
    Button btn_pay;

    String[] mTitle;
    String[] mDescription;
//        String mTitle[] = {"Big Mac®", "Chicken McNuggets®", "McSpicy® Chicken Filet", "Filet-O-Fish™",
//            "McWings® (4pcs)", "Double Cheeseburger™", "Sausage McMuffin® with Egg", "Caesar Salad"};
//    String mDescription[] = {"Big Mac® & Fires & Drink", "Chicken McNuggets® & Fires & Drink", "McSpicy® Chicken Filet & Fires & Drink", "Filet-O-Fish™ & Fires & Drink",
//            "McWings® (4pcs) & Fires & Drink", "Double Cheeseburger™ & Fires & Drink", "Sausage McMuffin® with Egg & Fires & Drink", "Caesar Salad & Fires & Drink"};
    int image[] = {R.drawable.set_1, R.drawable.set_2, R.drawable.set_3, R.drawable.set_4, R.drawable.set_5, R.drawable.set_6, R.drawable.set_7, R.drawable.set_8};

    ArrayList<String> selectedItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_menu);

        String shop = getIntent().getStringExtra("Shop");

        Resources res = getResources();
        mTitle = res.getStringArray(R.array.foodTitle);
        mDescription = res.getStringArray(R.array.foodDescription);

        foodList = findViewById(R.id.foodList);
        btn_pay = findViewById(R.id.pay);

        final Executor executor = Executors.newSingleThreadExecutor();
        final FoodMenuActivity biometricLoginActivity = this;
        final BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(this)
                .setTitle(getString(R.string.fingerprintTitle))
                .setSubtitle(getString(R.string.fingerprintSubtitle))
                .setDescription(getString(R.string.fingerprintDescription))
                .setNegativeButton(getString(R.string.fingerprintButton)
                        , executor, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }).build();

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                biometricPrompt.authenticate(new CancellationSignal(), executor, new BiometricPrompt.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
                        biometricLoginActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(FoodMenuActivity.this, BillActivity.class);
                                intent.putExtra("shop", shop);
                                intent.putExtra("foodBill", selectedItems);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });

        Food food = new Food(this, shop, mTitle, mDescription, image);
        foodList.setAdapter(food);

        foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    printer(position);
                }
                if (position == 1) {
                    printer(position);
                }
                if (position == 2) {
                    printer(position);
                }
                if (position == 3) {
                    printer(position);
                }
                if (position == 4) {
                    printer(position);
                }
                if (position == 5) {
                    printer(position);
                }
                if (position == 6) {
                    printer(position);
                }
                if (position == 7) {
                    printer(position);
                }
            }
        });

    }

    public void printer(int position) {
//        if (selectedItems.contains(mTitle[position])) {
//            selectedItems.remove(mTitle[position]);
//        } else {
            selectedItems.add(mTitle[position]);
//        }
        Toast.makeText(FoodMenuActivity.this, getString(R.string.added) + " " + mTitle[(position)], Toast.LENGTH_LONG).show();
        String items = "";
        for (String item: selectedItems) {
            items += "-" + item + "\n";
        }
        System.out.println(items);
    }

    class Food extends ArrayAdapter<String> {
        Context context;
        String shop;
        String title[];
        String description[];
        int imgs[];

        Food(Context context, String shop, String title[], String description[], int imgs[]) {
            super(context, R.layout.food_row, R.id.title, title);
            this.context = context;
            this.shop = shop;
            this.title = title;
            this.description = description;
            this.imgs = imgs;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.food_row, parent, false);
            ImageView images = row.findViewById(R.id.foodImage);
            TextView myTitle = row.findViewById(R.id.title);
            TextView myDescription = row.findViewById(R.id.description);

            images.setImageResource(imgs[position]);
            myTitle.setText(title[position]);
            myDescription.setText(description[position]);

            return row;
        }
    }
}